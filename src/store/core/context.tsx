import React, { createContext, useReducer, useContext, Context, useEffect } from 'react';
import { initialState } from '../initialState';
import { IStoreContext, IStoreProvider } from './types';
import { rootReducer } from '../rootReducer';

const StoreContext: Context<IStoreContext> = createContext({
  dispatch: (action: { type: string; payload?: any }) => {},
  state: initialState,
});

export const StoreProvider = ({ children }: IStoreProvider) => {
  const [state, dispatch] = useReducer(rootReducer, initialState);

  useEffect(() => {
    dispatch({ type: '@@INIT' });
  }, []);

  return <StoreContext.Provider value={{ state, dispatch }}>{children}</StoreContext.Provider>;
};

export const useStore = (): IStoreContext => {
  const { state, dispatch } = useContext(StoreContext);
  return { state, dispatch };
};
