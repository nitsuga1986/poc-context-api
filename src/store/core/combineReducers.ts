import { initialState } from '../initialState';
import { IAction, IStateApp } from './types';

export const combineReducers = (reducer: { [key: string]: Function }) => {
  return (state: IStateApp | any = initialState, action: IAction): IStateApp => {
    const keys = Object.keys(reducer);
    const nextReducers: any = {};
    for (let i = 0; i < keys.length; i++) {
      const invoke = reducer[keys[i]](state[keys[i]], action);
      nextReducers[keys[i]] = invoke;
    }
    return nextReducers;
  };
};
