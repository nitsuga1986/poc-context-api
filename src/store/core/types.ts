import { ReactNode } from 'react';
import { ICartItemProps } from '../../components/CartItem';

interface ICart {
  order: ICartItemProps[];
}

export interface IStateApp {
  shopping: ICart;
}
export declare type DispatchType = (action: IAction) => void;
export declare type DispatchActionType = (state: IStateApp, action: IAction) => IStateApp;
export interface IAction {
  type: string;
  payload?: unknown[] | object;
}
export interface IStoreProvider {
  children: ReactNode;
}
export interface IStoreContext {
  state: IStateApp;
  dispatch: DispatchType;
}
