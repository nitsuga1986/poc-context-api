import { useEffect, useState } from 'react';

interface IState {
  response: object[];
  loading: boolean;
}

const useFetchList = () => {
  const [fetching, setFetching] = useState<IState>({
    loading: false,
    response: [],
  });

  useEffect(() => {
    setFetching({ ...fetching, loading: true });

    (async () => {
      const response = await fetch('https://picsum.photos/v2/list?page=1&limit=25');
      const data = await response.json();
      setFetching({ ...fetching, response: data, loading: false });
    })();
  }, []);

  return fetching;
};

export default useFetchList;
