import { CartAction, CartActions } from './types';
import { ICartItemProps } from '../../components/CartItem';

export const reducers = (state: any = { order: [] }, action: CartActions) => {
  switch (action.type) {
    case CartAction.ADD_TO_CART:
      return !state.order.some((order: ICartItemProps) => order.id === action.payload.id)
        ? { ...state, order: [...state.order, action.payload] }
        : { ...state };
    case CartAction.REMOVE_TO_CART:
      return {
        ...state,
        order: [...state.order.filter((order: ICartItemProps) => order.id !== action.payload.id)],
      };
  }

  return { order: [] };
};
