import { useStore } from '../core/context';

import { CartAction } from './types';

const useCartActions = () => {
  const { state, dispatch } = useStore();

  const addToCart = (product: any) => {
    dispatch({ type: CartAction.ADD_TO_CART, payload: product });
  };

  const removeToCart = (product: any) => {
    dispatch({ type: CartAction.REMOVE_TO_CART, payload: product });
  };

  return { cart: state.shopping, addToCart, removeToCart };
};

export default useCartActions;
