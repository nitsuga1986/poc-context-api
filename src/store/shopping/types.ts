export enum CartAction {
  ADD_TO_CART = '@SHOPPING/ADD_TO_CART',
  REMOVE_TO_CART = '@SHOPPING/REMOVE_TO_CART',
}

export interface AddToCart {
  type: CartAction.ADD_TO_CART;
  payload: any;
}

export interface RemoveToCart {
  type: CartAction.REMOVE_TO_CART;
  payload: any;
}

export type CartActions = AddToCart | RemoveToCart;
