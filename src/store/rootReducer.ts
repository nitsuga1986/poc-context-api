import { combineReducers } from './core/combineReducers';
import { reducers as shoppingReducer } from './shopping/reducers';

export const rootReducer = combineReducers({
  shopping: shoppingReducer,
});
