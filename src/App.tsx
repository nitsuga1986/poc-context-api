import React from 'react';
import './App.css';

import { Cart, Footer, Logo, TopBar } from './components';
import { Home } from './pages/home';

const App: React.FC = () => {
  return (
    <div className="App">
      <TopBar>
        <Logo />
        <Cart />
      </TopBar>

      <Home />

      <Footer />
    </div>
  );
};

export default App;
