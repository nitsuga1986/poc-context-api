import React from 'react';

import { Hero, Item, List } from '../components';
import useFetchList from '../store/hooks/useFetch';

export const Home = () => {
  const { response } = useFetchList();

  const products = response.map((item: any) => ({
    description: item.url,
    id: item.id,
    image: item.download_url,
    price: item.height,
    title: item.author,
  }));

  return (
    <>
      <Hero />
      <List>
        {products.map(product => (
          <Item key={product.id} {...product} />
        ))}
      </List>
    </>
  );
};
