import { Col, Icon } from 'antd';

import React from 'react';
import styled from 'styled-components';

const Label = styled.label`
  font-weight: bold;
  font-size: 20px;
  margin-left: 10px;
  em {
    font-weight: normal;
    font-size: 14px;
    font-style: normal;
    margin-left: 4px;
  }
`;

const StyledCol = styled(Col)`
  display: flex;
  align-items: center;
`;

export const Logo = () => (
  <StyledCol span={4}>
    <Icon style={{ fontSize: '30px' }} type="experiment" />
    <Label>
      POC<em>api</em>
    </Label>
  </StyledCol>
);
