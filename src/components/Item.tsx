import BackgroundImage from 'react-background-image';
import loading from '../assets/loading.svg';

import React from 'react';
import styled from 'styled-components';

import { Button, Col } from 'antd';
import useCartActions from '../store/shopping/actions';

interface IProps {
  id: string;
  title: string;
  description: string;
  image: string;
  price: number;
}

const StyledItem = styled(Col)`
  background: white;
  padding: 10px 20px;
  figure {
    width: 100%;
    overflow: hidden;
    margin: 0;
    height: 280px;
    img {
      display: block; /*remove inline-block spaces*/
      margin: 0 -38.885%;
      width: 177.777%;
    }
  }
`;

const Title = styled.span`
  color: #1f1f1f;
  font-weight: bold;
  display: block;
  padding: 10px 0;
  text-transform: uppercase;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  display: block;
`;

const Description = styled.span`
  color: #a4a0a0;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  display: block;
`;

const Price = styled.span`
  color: #1f1f1f;
  font-weight: bold;
  padding: 10px 0;
  text-transform: uppercase;
  display: inline-block;
  width: 50%;
`;

const Cart = styled.span`
  padding: 10px 0;
  display: inline-block;
  width: 50%;
  text-align: right;
`;

export const Item: React.FC<IProps> = ({ id, title, description, image, price }: IProps) => {
  const { addToCart } = useCartActions();

  return (
    <StyledItem key={id} xs={12} md={6}>
      <figure>
        <BackgroundImage placeholder={loading} src={image} />
      </figure>
      <Title>{title}</Title>
      <Description>{description}</Description>
      <Price>{(price / 100).toFixed(2)} €</Price>
      <Cart>
        <Button
          size="large"
          type="primary"
          shape="circle"
          icon="shopping-cart"
          onClick={() => {
            addToCart({ id, title, description, image, price });
          }}
        />
      </Cart>
    </StyledItem>
  );
};
