import { Divider, Row } from 'antd';
import { Container } from '.';

import React from 'react';
import styled from 'styled-components';

interface IProps {
  children: React.ReactNode;
}

const StyledDivider = styled(Divider)`
  margin: 40px 0;

  span {
    font-weight: bold;
    color: #14194c;
  }
`;

export const List: React.FC<IProps> = ({ children }: React.Props<unknown>) => (
  <main>
    <Container>
      <StyledDivider>Listado de productos</StyledDivider>
      <Row>{children}</Row>
    </Container>
  </main>
);
