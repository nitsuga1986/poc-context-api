import { Row } from 'antd';
import { Container } from '.';

import React from 'react';
import styled from 'styled-components';

interface IProps {
  children: React.ReactNode;
}

const Header = styled.header`
  position: fixed;
  width: 100%;
  z-index: 3;
  background: #3747df;
  padding-top: 20px;
  padding-bottom: 20px;
  color: white;
`;

export const TopBar: React.FC<IProps> = ({ children }: React.Props<unknown>) => (
  <Header>
    <Container>
      <Row type="flex" justify="space-between">
        {children}
      </Row>
    </Container>
  </Header>
);
