import image from '../assets/picture.svg';

import React from 'react';
import styled from 'styled-components';
import { Container } from '.';

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  margin: 100px 0 25px 0;
`;

const Label = styled.label`
  color: #12174e;
  font-size: 45px;
  font-weight: bold;
  width: 75%;
`;

const Secondary = styled.span`
  color: #3747db;
  font-size: 60px;
  float: left;
`;

const Image = styled.img`
  width: 35%;
`;

export const Hero = () => {
  return (
    <Container>
      <Wrapper>
        <Label>
          Proof of concept using
          <Secondary> React API Context</Secondary>
        </Label>

        <Image alt="hero" src={image} />
      </Wrapper>
    </Container>
  );
};
