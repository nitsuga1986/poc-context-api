import React from 'react';
import styled from 'styled-components';
import { Container } from '.';

const StyledFooter = styled.footer`
  color: #9a9a9a;
  text-align: center;
  padding: 15px 10px;
`;

export const Footer = () => (
  <StyledFooter>
    <Container>Proof of Concept - React Context API | {new Date().getFullYear()}</Container>
  </StyledFooter>
);
