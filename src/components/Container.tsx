import React from 'react';

const style = {
  margin: '0 auto',
  maxWidth: '1180px',
  padding: '0 20px',
};

interface IProps {
  children: React.ReactNode;
}

export const Container: React.FC<IProps> = ({ children }: React.Props<unknown>) => (
  <div style={style}>{children}</div>
);
