import { Badge, Col, Icon, Drawer } from 'antd';
import styled from 'styled-components';

import React, { useState } from 'react';
import { useStore } from '../store/core/context';
import { CartItem } from '.';
import { ICartItemProps } from './CartItem';

const StyledCol = styled(Col)`
  text-align: right;
  cursor: pointer;
`;

const ClickCart = styled.a`
  color: white;
  &:hover {
    color: white;
  }
`;

const CartList = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0;
  text-align: right;
`;

export const Cart = () => {
  const [cartVisible, setCartVisible] = useState(false);
  const { state } = useStore();
  const { order } = state.shopping;
  let total = 0;
  order.forEach((item: ICartItemProps) => (total += item.price / 100));

  const shoppingList = (
    <CartList>
      {order.map((product: ICartItemProps) => (
        <CartItem
          key={product.id}
          id={product.id}
          image={product.image}
          title={product.title}
          price={product.price}
        />
      ))}
      total: <b>{total.toFixed(2)}€</b>
    </CartList>
  );

  return (
    <StyledCol span={4}>
      <ClickCart onClick={() => setCartVisible(true)}>
        <Badge count={order.length}>
          <Icon style={{ fontSize: '30px' }} type="shopping-cart" />
        </Badge>
      </ClickCart>
      <Drawer
        width={'50%'}
        title="My Cart"
        placement="right"
        visible={cartVisible}
        onClose={() => setCartVisible(false)}
      >
        {shoppingList}
      </Drawer>
    </StyledCol>
  );
};
