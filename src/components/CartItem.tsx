import React from 'react';
import styled from 'styled-components';
import { Divider, Button } from 'antd';
import useCartActions from '../store/shopping/actions';

export interface ICartItemProps {
  id: string;
  image: string;
  title: string;
  price: number;
}

const Image = styled.img`
  object-fit: cover;
  width: 75px;
  height: 75px;
`;

const Item = styled.li`
  margin: 5px 0;
  display: flex;
  justify-content: space-between;
  align-items: center;
  label {
    width: 150px;
  }
  span {
    font-weight: bold;
    &:after {
      content: '€';
    }
  }
`;

const Picture = styled.div`
  position: relative;
`;
const StyledButton = styled(Button)`
  position: absolute;
  left: 14px;
  bottom: -17px;
`;

export const CartItem: React.FC<ICartItemProps> = ({ ...product }) => {
  const { removeToCart } = useCartActions();
  return (
    <div key={product.id}>
      <Item>
        <Picture>
          <Image src={product.image} />
          <StyledButton shape="round" icon="delete" onClick={() => removeToCart(product)} />
        </Picture>

        <label>{product.title}</label>
        <span>{(product.price / 100).toFixed(2)}</span>
      </Item>
      <Divider />
    </div>
  );
};
